package com.natife.psycho.data.db

import android.arch.persistence.room.TypeConverter
import com.natife.psycho.model.Gender

class GenderTypeConverter {

    @TypeConverter
    fun fromDb(value: String?): Gender? {
        return value?.let { Gender.valueOf(value) }
    }

    @TypeConverter
    fun toDb(value: Gender?): String? {
        return value?.name
    }
}
