package com.natife.psycho.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import com.natife.psycho.model.User

@Database(entities = [User::class], version = 1)
@TypeConverters(GenderTypeConverter::class)
abstract class MyDatabase : RoomDatabase() {

    abstract fun getUsersDao(): UsersDao
}