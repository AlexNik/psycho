package com.natife.psycho.datamanager

import com.natife.psycho.data.AppExecutors
import com.natife.psycho.data.db.MyDatabase
import com.natife.psycho.model.User

interface UserDataManager {

    fun createProfile(user: User)
}

class UserDataManagerImpl constructor(val database: MyDatabase, val appExecutors: AppExecutors): UserDataManager {

    override fun createProfile(user: User) {
        database.getUsersDao().insertUser(user)
    }
}