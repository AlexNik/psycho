package com.natife.psycho.di

import android.arch.persistence.room.Room
import com.natife.psycho.data.AppExecutors
import com.natife.psycho.data.db.MyDatabase
import com.natife.psycho.datamanager.UserDataManager
import com.natife.psycho.datamanager.UserDataManagerImpl
import com.natife.psycho.ui.main.MainViewModel
import org.koin.android.architecture.ext.viewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.applicationContext

private val appModule = applicationContext {
    viewModel { MainViewModel(get()) }
    bean { AppExecutors() }
    bean { UserDataManagerImpl(get(), get()) as UserDataManager }
}

private val dbModule = applicationContext {
    bean { Room.databaseBuilder(androidApplication(), MyDatabase::class.java, "PsychoDb").build() as MyDatabase }
}

val moduleList = listOf(appModule, dbModule)