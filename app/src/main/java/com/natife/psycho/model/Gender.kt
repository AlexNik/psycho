package com.natife.psycho.model

enum class Gender {
    MALE,
    FEMALE
}