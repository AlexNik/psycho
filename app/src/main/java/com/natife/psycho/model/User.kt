package com.natife.psycho.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "USER")
data class User(

        @PrimaryKey
        @ColumnInfo(name = "ID")
        var id: Long? = null,

        @ColumnInfo(name = "NAME")
        @SerializedName("name")
        var name: String? = null,

        @ColumnInfo(name = "GENDER")
        @SerializedName("gender")
        var gender: Gender? = null,

        @Embedded(prefix = "BIRTH_")
        @SerializedName("birth_day")
        var birthDay: BirthDay? = null,

        @ColumnInfo(name = "AVATAR")
        @SerializedName("avatar")
        var avatar: String? = null
) {
        data class BirthDay(
                @ColumnInfo(name = "DAY")
                @SerializedName("day")
                var day: Int,

                @ColumnInfo(name = "MONTH")
                @SerializedName("month")
                var month: Int,

                @ColumnInfo(name = "YEAR")
                @SerializedName("year")
                var year: Int
        )
}