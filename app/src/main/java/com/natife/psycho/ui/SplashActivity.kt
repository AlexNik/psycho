package com.natife.psycho.ui

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.natife.psycho.ui.main.MainActivity

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Handler().postDelayed({
            MainActivity.start(SplashActivity@this)
            finish()
        }, 1000)
    }

    override fun onBackPressed() {

    }
}
