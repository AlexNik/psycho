package com.natife.psycho.ui.custom

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.request.RequestOptions
import com.natife.psycho.R
import com.natife.psycho.utils.DimensUtils

class AvatarView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private val outerCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val innerCirclePaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val innerCirclePadding = DimensUtils.dpToPx(8f)
    private val imageViewPadding = DimensUtils.dpToPx(12f)
    private var outerCircleRadius = 0f
    private var innerCircleRadius = 0f
    private var cx = 0f
    private var cy = 0f

    private val imageView = ImageView(context)

    init {
        setWillNotDraw(false)

        outerCirclePaint.style = Paint.Style.FILL
        outerCirclePaint.color = ContextCompat.getColor(context, R.color.colorAccentTransparent)

        innerCirclePaint.style = Paint.Style.FILL
        innerCirclePaint.color = ContextCompat.getColor(context, R.color.colorAccent)

        imageView.setPadding(imageViewPadding, imageViewPadding, imageViewPadding, imageViewPadding)
        imageView.setImageResource(R.drawable.profile)
        addView(imageView)
    }

    fun setImage(image: String?) {
        Glide.with(this)
                .load(image)
                .apply(RequestOptions().apply {
                    transform(CircleCrop())
                    placeholder(R.drawable.profile)
                })
                .into(imageView)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        outerCircleRadius = Math.min(measuredHeight, measuredWidth) / 2f
        innerCircleRadius = Math.min(measuredHeight, measuredWidth) / 2f - innerCirclePadding
        cx = measuredWidth / 2f
        cy = measuredHeight / 2f
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        canvas.drawCircle(cx, cy, outerCircleRadius, outerCirclePaint)
        canvas.drawCircle(cx, cy, innerCircleRadius, innerCirclePaint)
    }
}
