package com.natife.psycho.ui.main

import android.app.Activity
import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import com.natife.psycho.R
import com.natife.psycho.base.BaseActivity
import com.natife.psycho.model.Gender
import com.natife.psycho.utils.FileUtils
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.architecture.ext.viewModel
import java.io.File
import java.util.*

class MainActivity : BaseActivity<MainViewModel>() {

    private val viewModel by viewModel<MainViewModel>()

    private var tempPhotoFile: File? = null

    companion object {
        private const val RC_PICK_IMAGE = 0
        private const val RC_IMAGE_CAPTURE = 1
        private const val RC_READ_EXTERNAL_STORAGE = 2
        private const val RC_CAMERA_PERMISSION = 3

        fun start(activity: Activity) {
            activity.startActivity(Intent(activity, MainActivity::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        avatarImageView.setOnClickListener {
            showAvatarSelectionDialog()
        }

        birthBtn.setOnClickListener {
            val calendar = Calendar.getInstance()
            val dialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                viewModel.onDateOfBirthSelected(year, month, dayOfMonth)
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
            dialog.show()
        }

        genderGroup.setOnCheckedChangeListener { _, checkedId ->
            when(checkedId) {
                R.id.maleBtn -> viewModel.onGenderSelected(Gender.MALE)
                R.id.femaleBtn -> viewModel.onGenderSelected(Gender.FEMALE)
            }
        }

        nameEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                viewModel.onNameSelected(s.toString())
            }
        })

        createBtn.setOnClickListener {
            viewModel.onCreateProfileClicked()
        }

        viewModel.canCreateProfile.observe(this, Observer {
            createBtn.isEnabled = it?: false
        })

        viewModel.user.observe(this, Observer {
            birthBtn.text = it?.birthDay?.let {
                "${it.day}/${it.month}/${it.year}"
            }
            avatarImageView.setImage(it?.avatar)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            RC_PICK_IMAGE -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.onAvatarSelected(FileUtils.getPathFromUri(this, data?.data))
                }
            }
            RC_IMAGE_CAPTURE -> {
                if (resultCode == Activity.RESULT_OK) {
                    viewModel.onAvatarSelected(tempPhotoFile?.absolutePath)
                }
            }
        }
    }

    private fun showAvatarSelectionDialog() {
        AlertDialog.Builder(this)
                .setItems(arrayOf(getString(R.string.pick_image), getString(R.string.take_photo))) { _, which ->
                    when(which) {
                        0 -> onPickImageClicked()
                        1 -> onTakePhotoClicked()
                    }
                }
                .show()
    }

    private fun onTakePhotoClicked() {
        if (checkCameraPermission()) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                tempPhotoFile = FileUtils.createImageFile(this)
                val temp = tempPhotoFile

                if (temp != null) {
                    val photoURI = FileProvider.getUriForFile(this,
                            getString(R.string.file_provider_name),
                            temp)
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, RC_IMAGE_CAPTURE)
                }
            }
        } else {
            requestCameraPermission(RC_CAMERA_PERMISSION)
        }
    }

    private fun onPickImageClicked() {
        if (checkStoragePermissions()) {
            startActivityForResult(Intent.createChooser(Intent().apply {
                type = "image/*"
                action = Intent.ACTION_GET_CONTENT
            }, getString(R.string.select_picture)), RC_PICK_IMAGE)
        } else {
            requestStoragePermissions(RC_READ_EXTERNAL_STORAGE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            RC_READ_EXTERNAL_STORAGE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionStorageGranted()
                }
            }
            RC_CAMERA_PERMISSION -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onPermissionCameraGranted()
                }
            }
        }
    }

    private fun onPermissionCameraGranted() {
        onTakePhotoClicked()
    }

    private fun onPermissionStorageGranted() {
        onPickImageClicked()
    }
}