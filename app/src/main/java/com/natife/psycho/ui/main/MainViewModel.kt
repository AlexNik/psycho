package com.natife.psycho.ui.main

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import com.natife.psycho.base.BaseViewModel
import com.natife.psycho.datamanager.UserDataManager
import com.natife.psycho.model.Gender
import com.natife.psycho.model.User
import timber.log.Timber

class MainViewModel constructor(val userDataManager: UserDataManager): BaseViewModel() {

    val user = MutableLiveData<User>().apply {
        value = User().apply {
            gender = Gender.MALE
        }
    }

    val canCreateProfile: LiveData<Boolean> = Transformations.map(user) {
        !it?.name.isNullOrBlank() && it.birthDay != null && it.gender != null
    }

    fun onAvatarSelected(avatarPath: String?) {
        user.value = user.value?.also {
            it.avatar = avatarPath
        }
    }

    fun onDateOfBirthSelected(year: Int, month: Int, dayOfMonth: Int) {
        user.value = user.value?.also {
            it.birthDay = User.BirthDay(dayOfMonth, month, year)
        }
    }

    fun onGenderSelected(gender: Gender) {
        user.value = user.value?.also {
            it.gender = gender
        }
    }

    fun onNameSelected(name: String) {
        user.value = user.value?.also {
            it.name = name
        }
    }

    fun onCreateProfileClicked() {
        Timber.d("onCreateProfileClicked $user")
    }
}